# README #
Homework contains two automated tests which are checking two cases:
	1) Error on empty email
	2) Error on incorrect email
All of these test cases are checking highlights on error

### What is this repository for? ###

* Homework for 'Wissenrieder'
* Version 1.0-SNAPSHOT


### How do I get set up? ###

* Java 11 and Maven 3 
* JAVA_HOME and M3_HOME should be setted into PATH (only for windows)
* Download FF, CHROME or IE driver and add path into corresponding capability file, or system variable
* How to run tests: ### mvn -P firefox,localhost,nogrid test  ###

