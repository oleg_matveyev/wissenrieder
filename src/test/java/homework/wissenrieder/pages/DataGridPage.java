package homework.wissenrieder.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

/**
 * Sample page
 */
public class DataGridPage extends Page {

  @FindBy(how = How.TAG_NAME, using = "h1")
  @CacheLookup
  public WebElement header;

  @CacheLookup
  @FindBy(how = How.ID, using = "gridContainer")
  public WebElement dataGridDiv;

  @FindBy(how = How.CLASS_NAME, using = "load-panel")
  public WebElement loadPanelDiv;

  @FindBy(how = How.CLASS_NAME, using = "dx-datagrid-addrow-button")
  public WebElement addRecordButton;

  @FindBy(how = How.ID, using = "demoFrame")
  public WebElement demoFrame;

  @FindBy(how = How.XPATH, using = "//*[@class=\"dx-datagrid-content\"]/table/tbody/tr[1]/td[5]/div/div/div/div[1]/input")
  public WebElement emailTxt;

  @FindBy(how = How.XPATH, using = "//*[@class=\"dx-datagrid-content\"]/table/tbody/tr[1]/td[5]")
  public WebElement emailTd;

  @FindBy(how = How.CLASS_NAME, using = "dx-datagrid-save-button")
  public WebElement saveButton;

  @FindBy(how = How.XPATH, using = "//*[@class=\"dx-datagrid-content\"]/table/tbody/tr[1]/td[5]/div[2]/div/div")
  public WebElement warningDiv;

  @FindBy(how = How.XPATH, using = "//*[@class=\"dx-datagrid-content\"]/table/tbody/tr[1]/td[5]/div[1]/div")
  public WebElement emailDiv;



  public DataGridPage(WebDriver webDriver) {
    super(webDriver);
  }
}
