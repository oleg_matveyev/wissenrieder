package homework.wissenrieder;

import org.openqa.selenium.Keys;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.*;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import homework.wissenrieder.pages.DataGridPage;

import java.security.Key;

public class DataGridTest extends TestNgTestBase {

  private DataGridPage homepage;

  @BeforeMethod
  public void initPageObjects() {
    homepage = PageFactory.initElements(driver, DataGridPage.class);
  }

  @Test
  public void testEmptyEmailFail(){
    navigateDataGrid();
    homepage.addRecordButton.click();
    fillEmail("");
    homepage.saveButton.click();
    checkWarning(Constants.EMAIL_REQUIRED_TEXT);
    isHightlighted();
  }

  @Test
  public void testInvalidEmailFail(){
    navigateDataGrid();
    homepage.addRecordButton.click();
    fillEmail("incorrectEmail");
    homepage.saveButton.click();
    checkWarning(Constants.INVALID_EMAIL_TEXT);
    isHightlighted();
  }

  private void checkWarning(String warningText){
    homepage.emailTd.click();
    Assert.assertEquals(homepage.warningDiv.getText(), warningText);
  }

  private void navigateDataGrid(){
    driver.get(baseUrl);
    WebDriverWait wait = new WebDriverWait(driver,30);
    wait.until(ExpectedConditions.attributeToBe(homepage.loadPanelDiv,"style", "display: none;"));
    driver.switchTo().frame(homepage.demoFrame);
    Assert.assertTrue(homepage.dataGridDiv.isDisplayed(), "DataGrid not found!");
  }

  private void fillEmail(String email)
  {
    homepage.emailTd.click();
    homepage.emailTxt.sendKeys(email);
  }

  private void isHightlighted(){
    Assert.assertTrue(homepage.emailDiv.getAttribute("class").contains("dx-show-invalid-badge"), "Email field is not highlighted!"); // move css class to contact
  }

}
