package homework.wissenrieder;

public final class Constants {
    public static final String INVALID_EMAIL_TEXT = "Email is invalid";
    public static final String EMAIL_REQUIRED_TEXT= "The Email field is required.";
}
